/*
 * File:   main.cpp
 * Author: wrossmck
 *
 * Created on 12 July 2012, 17:33
 */

#include <cstdlib>
#include <iostream>
//Variables:
//int Number:To save the Input number and the processed number.
//int itr:To count the number of iterations
//int arr[4]: To save the four digits of the number in individual memory spaces
//int numAsd and numDes:To save the number in Ascending and Descending order

using namespace std;
int Number, itr=1, numAsd, numDes=0,arr[4];

int BubbleSortArray()
{
    
	for(int count = 1; count<=4; count++)	//Loop to perform 4 sorting iterations on the array.
	{
		for(int j=0;j<3;j++)	//Loop to perform 4 comparisons in each iteration.
		{
			if(arr[j]>arr[j+1])	//Swaps the numbers if 1st Number is larger than the 2nd.
			{
				swap(arr[j],arr[j+1]);
			}
		}
	}
	return 0;
}

int main()
{
    
	cout<<"Kaprekar's Constant (6174) Calculator."<<endl<<endl;
    
	cout<<"Enter a 4-Digit number: ";
	cin>>Number;
	do		//The do while loop which runs till the final number becomes = 6174
	{
		cout<<"->Iteration number "<<itr<<endl<<endl;
		numAsd=0,numDes=0;	//Initialize to 0 at the start of each iteration
        
		//Save the individual digits of the number to individual locations in the array
		for(int count=0;count<4;++count)
		{
			arr[count]=Number%10;
			Number=Number/10;
		}
        
        
		BubbleSortArray(); //Sort the individual Digits in the array in ascending order
        
		cout<<"The Ascending form of the number = ";
		for(int count=0;count<4;count++)	//Convert the individual digits in the array into an Ascending ordered number
		{
			numAsd=(numAsd*10)+arr[count];
			cout<<arr[count];
		}
        
		cout<<endl;
        
		cout<<"The Descending form of the number = ";
		for(int count=3;count>=0;--count)	//Convert the individual digits in the array into a Descending ordered number
		{
			numDes=(numDes*10)+arr[count];
			cout<<arr[count];
		}
		cout<<endl;
        
		Number=numDes-numAsd; //Subtract the Ascending ordered number from descending ordered number
		cout<<"Their Difference: "<<numDes<<"-"<<numAsd<<"="<<Number<<endl<<endl;
		itr++;
        
	}while(Number!=6174);
    
	cout<<"Thus Kaprekar's constant 6174 was found in "<<itr-1<<" Iterations.";
    
	cin.get();
	return 0;
}